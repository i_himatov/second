var sumA = function(from, to){
	if (from > to) {
		console.log('Second argument must be bigger than first argument');
		return false;
	}

	var res = 0;
	for (var i = from; i <= to; i++){
		if (i % 2 == 0 && i % 3 == 0)
			res += i;
	}

	console.log('sumA = ' + res);
}

sumA(0,20);

var sumB = function(){
	var res = 0;
	for (var i = 0; i < arguments.length; i++) {
		res += arguments[i];
	}

	console.log('sumB = ' + res);
}

sumB(7,5,8,10);

var sumC = function(n){
	if (n == 1)
		return 1;
	return n + sumC(n - 1);
}

console.log('sumC = ' + sumC(100));